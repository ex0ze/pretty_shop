-- тип валюты - нужен для оценки продуктов (относительно доллара)
-- думаю, эти таблицы должны в N часов M минут каждого дня обновляться с какого-нибудь онлайн ресурса
create table if not exists "currency" (
	"id" serial primary key,
	"title" text not null, -- ну пусть будет Рубль
	"code" char(3) -- "RUB"
);


create table if not exists "currency_rate"
(
	"id" serial primary key,
	"ts" timestamp with time zone not null, -- временная метка обновления курса валюты
	"dollar_relation" double precision not null, -- отношение валюты к доллару (на сколько нужно умножить один доллар чтобы получить УЕ валюты)
	"currency_id" integer, -- id валюты
	foreign key ("currency_id") references "currency"("id") on delete cascade,
	UNIQUE("ts", "dollar_relation")
);

-- id 1
-- title "Доллар"
-- dollar_relation 1.00

--id 2
--title "Рубль"
-- dollar_relation 0.013

-- думаю, нужно захардкодить таблицы с долларом, рублём и например гривной,
-- сделать процедуры обновления

-- insert into currency(title, code) VALUES ('Рубль', 'RUB');
-- select * from currency;
-- select * from currency_rate;
-- insert into currency_rate(ts, dollar_relation, currency_id) VALUES (now(), 0.013, 1);
-- select title, code, cr.ts, cr.dollar_relation from currency left join currency_rate cr on currency.id = cr.currency_id order by ts desc limit 1;
-- delete from currency_rate where ts < now();
-- drop table currency;