create table if not exists "unit_group" -- группа единиц измерений (объем, масса, мощность и т.д.)
(
	"id" serial primary key,
	"title" text not null, -- например Масса
	UNIQUE("title")
);

create table if not exists "unit" -- единица измерения
(
    "id" serial primary key,
    "title" text not null, -- например Килограмм
    "power" smallint not null, -- степень 10(Килограмм это 1000 грамм, значит здесь будет 3
    "unit_group_id" integer not null, --например id на unit_group "масса"
    foreign key ("unit_group_id") references "unit_group"("id") on delete cascade,
    UNIQUE ("unit_group_id", "power") -- уникальность - связка идентификатора группы + степени
);

--select unit.title, unit.power, ug.title from unit join unit_group ug on unit.unit_group_id = ug.id;
