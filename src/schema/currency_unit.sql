-- таблица служит связующим звеном между величиной (килограмм) и валютой (рубль)
-- в результате получаем отношение например рубль за килограмм
-- будет использоваться в Продукте, где продукт будет иметь поле Цена(double) и ссылку на эту таблицу
create table if not exists "currency_unit"
(
    "id" serial primary key,
    "unit_id" integer, --айди величины (например айди Килограмма)
    "currency_id" integer, -- айди валюты (например, рубль) (курс валют будет вычисляться в дальнейшем)
    foreign key ("unit_id") references "unit"("id") on delete cascade,
    foreign key ("currency_id") references "currency"("id") on delete cascade,
    UNIQUE("unit_id", "currency_id")
);

-- insert into currency_unit(unit_id, currency_id) VALUES (5, 1);
-- select * from currency_unit;
-- select u.title, c.title from currency_unit join currency c on c.id = currency_unit.currency_id join unit u on u.id = currency_unit.unit_id;
-- delete from unit;

