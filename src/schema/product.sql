create table if not exists 'product'
(
	'id' serial primary key,
	'title' text,
	'unit_id' references unit(id), -- ссылка на количественную характеристику
	'price' money, -- цена за 
	'amount' real -- количество в наличии
);

create table if not exists 'internal_image'
(
	'id' serial primary key,
	'title' text not null,
	'binary_data' bytea
);

create table if not exists 'external_image'
(
	'id' serial primary key,
	'title' text not null,
	'url' text
);

create table if not exists 'product_image'
(
	'id serial' primary key,
	'product_id' integer references product(id),
	'image_id' integer references image(id)
);

create table if not exists 'cart'
(
	'id' serial primary key
);

create table if not exists 'product_cart'
(
	'id' serial primary key,
	'product_id' integer references product(id),
	'cart_id' integer references cart(id)
);

create table if not exists 'customer'
(
	'id' serial primary key,
	'username' varchar(255) not null,
	'cart_id' integer references cart(id)
);